package com.control.cn.model;

/**
 * 
 * @Description: 
 * @author wangzonghui
 * @date 2018年9月11日 下午3:35:01
 */
public class ServiceResult<T> {
	    //状态 0成功  1失败
		private long code;
		//失败时返回错误信息
		private String message;
		// 请求结果
		private T data;
		
		public ServiceResult() {
		}
		
		public ServiceResult(long code, String message, T data) {
			super();
			this.code = code;
			this.message = message;
			this.data = data;
		}
		
		public T getData() {
			return data;
		}

		public void setData(T data) {
			this.data = data;
		}

		public ServiceResult(long code, T data) {
			super();
			this.code = code;
			this.message = "";
			this.data = data;
		}

		public long getCode() {
			return code;
		}

		public void setCode(long code) {
			this.code = code;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

}
