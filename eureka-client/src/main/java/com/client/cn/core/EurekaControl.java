package com.client.cn.core;

import java.util.HashMap;

import javax.annotation.Resource;

import org.springframework.cloud.client.serviceregistry.ServiceRegistry;
import org.springframework.cloud.netflix.eureka.serviceregistry.EurekaRegistration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.client.cn.model.ServiceResult;

@RestController
@RequestMapping("eureka-client")
public class EurekaControl {
	
	@Resource
    private ServiceRegistry<EurekaRegistration> serviceRegistry;
    
    @Resource
    private EurekaRegistration registration;
    
    @RequestMapping(value = "status", method = RequestMethod.GET)
    public ServiceResult<HashMap> getStatus(){
    	return new ServiceResult<HashMap>(0l,"Success",serviceRegistry.getStatus(registration));
    }
    
    /**
     * UP, // Ready to receive traffic
        DOWN, // Do not send traffic- healthcheck callback failed
        STARTING, // Just about starting- initializations to be done - do not
        // send traffic
        OUT_OF_SERVICE, // Intentionally shutdown for traffic
        UNKNOWN;
     */
    @RequestMapping(value = "status", method = RequestMethod.POST)
    public ServiceResult<String> setStatus(String status){
    	serviceRegistry.setStatus(registration, status);  //
    	return new ServiceResult<String>(0l,"Success","");
    }

}
