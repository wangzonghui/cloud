package com.client.cn;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.discovery.DiscoveryManager;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class EurekaClinetApplication {
    public static void main(String[] args) {
        SpringApplication.run( EurekaClinetApplication.class, args );
    }

    @Value("${server.port}")
    String port;
    
    /**
     * 测试方法
     */
    @RequestMapping("/hi")
    public String home(@RequestParam(value = "name", defaultValue = "forezp") String name) {
        return "hi " + name + " ,i am from port:" + port;
    }
    
    /**
     * 主动下线
     */
    @RequestMapping(value="/offline",method=RequestMethod.GET)
    public void offline() {
    	DiscoveryManager.getInstance().shutdownComponent();
    }

}
