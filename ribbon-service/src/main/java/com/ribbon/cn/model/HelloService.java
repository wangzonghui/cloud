package com.ribbon.cn.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class HelloService {
	
	@Autowired
	RestTemplate restTemplate;

	/**
	 * 添加注解，增加熔断功能 
	 */
	@HystrixCommand(fallbackMethod="Error")
    public String hiService(String name) {
        return restTemplate.getForObject("http://EUREKA-CLIENT/hi?name="+name,String.class);
    }
	
	public String Error(String name) {
		return "hi,"+name+", request Error";
	}
}
