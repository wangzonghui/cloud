# cloud

#### 项目介绍
基于spring cloud的个人开发框架模板。

#### 软件架构
spring boot2.0+spring cloud Finchley.SR1
#### 模块
* eureka-service：微服务注册中心  
	* http://localhost:1000/
* eureka-client:服务提供者 
	* http://localhost:2000/hi?name=work
* ribbon-service:服务消费者，实现负载均衡  
	* http://localhost:3000/hi?name=work
* （有问题）feign-service:服务消费者，1、基于接口注解，2、整合ribbon，具有负载均衡能力 3、整合了Hystrix，具有熔断能力    
	* http://localhost:4000/hi?name=work
* zuul-server：主要负责路由转发和过滤器
	* （fegin-有问题）http://localhost:5000/api-a/hi?name=forezp
	* http://localhost:5000/api-b/hi?name=forezp
	* 过滤时，必须使用传递token值，http://localhost:5000/api-a/hi?name=forezp&token=2342
* Hystrix 服务容错保护，增加到ribbon中
	* 默认请求5秒20次
* springcloudconfig 公共git配置仓库
* config-service 分布式配置服务端
	* http://localhost:7000/foo/dev
* config-client 分布式配置客户端
	* http://localhost:7001/foo/dev
	* http请求地址和资源文件映射如下:
		* /{application}/{profile}[/{label}]
		* /{application}-{profile}.yml
		* /{label}/{application}-{profile}.yml
		* /{application}-{profile}.properties
		* /{label}/{application}-{profile}.properties
		
* bus:消息总线（未实现）
* sleuth:服务追踪



### 网络架构
* ribbon负载均衡和容错 -->zuul路由和过滤 --> 服务提供者

