package com.feign.cn.model;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "eureka-client")
public interface ModelService {
	
	 @RequestMapping(value = "/hi",method = RequestMethod.GET)
	 String model(@RequestParam(value = "name") String name);
}
