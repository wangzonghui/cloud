package com.feign.cn.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ModelController {

	@Autowired
    ModelService modelService;

    @GetMapping(value = "/hi")
    public String sayHi(@RequestParam String name) {
        return modelService.model( name );
    }

}
